#ifndef READTASK_H
#define READTASK_H

#include "MsgService.h"
#include "Task.h"

class ReadTask : public Task {

  public:

	explicit ReadTask();
	void init(int period);
	void tick() override;

  private:
	int prevTemp = 0;
};

#endif