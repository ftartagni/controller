#ifndef STATETYPE_H
#define STATETYPE_H

/**
 * Represents the Thermostat's state.
 */
enum StateType {
    
    /** Thermostat is off */
	OFF,

    /** Thermostat is running, Mobile App and the Controller's button can notify it (-> turning off)*/
    ON
};

#endif
