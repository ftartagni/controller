#ifndef TIMER_H
#define TIMER_H

/**
 * A generic timer.
 */
class Timer {
  public:
	/**
	 * Instanciates the timer.
	 */
	Timer();

	/**
	 * Set the timer's frequency.
	 * @param frequency timer frequency
	 */
	void setupFrequency(int frequency);

	/**
	 * Se the timer's period (in ms).
	 * @param period timer period (in ms)
	 */
	void setupPeriod(int period);

	/**
	 * Wait for next tick.
	 */
	void waitForNextTick();
};

#endif
