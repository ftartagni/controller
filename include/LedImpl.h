#ifndef LEDIMPL_H
#define LEDIMPL_H

#include "Led.h"
#include <Arduino.h>

/**
 * Represents a LED connected to a pin.
 */
class LedImpl : public Led {
	unsigned pin;
	bool isOn;

  public:
	/**
	 * Instanciates the LED.
	 * @param pin The pin in which the LED is connected.
	 */
	explicit LedImpl(unsigned pin);

	void switchOn() override;
	void switchOff() override;
	bool isTurnedOn() override;
};

#endif
