#ifndef BUTTONTASK_H
#define BUTTONTASK_H

#include "MsgService.h"
#include "Task.h"
#include "Led.h"

class ButtonTask : public Task {

  public:

	explicit ButtonTask(Led* led);
	void init(int period);
	void tick() override;

private:
	Led* led;
    int prevButtonState = 0;
	int currButtonState = 0;

};

#endif