#ifndef THERMOSTATGLOBAL_H
#define THERMOSTATGLOBAL_H

#include "Arduino.h"
#include "StateType.h"
#include "ProfileType.h"

class ThermostatGlobal {
  public:
    static const String dayProfileSymbol;
    static const String nightProfileSymbol;
    static const String TurnOnSymbol;
    static const String TurnOffSymbol;
    static float currTemp;
    static const float stepMax;
    static float dayMinTemp;
    static float nightMinTemp;
    static StateType state;
    static ProfileType profile;
    static const char endChar;
    static float baseTemp;
    static bool isForcedOff;
    static float profileTemps[];
};

#endif
