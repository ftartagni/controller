#ifndef MSGSERVICEBT_H
#define MSGSERVICEBT_H

#include "Arduino.h"
#include "SoftwareSerial.h"

class MsgBT{
  String content;

public:
  MsgBT(const String& content){
    this->content = content;
  }
  
  String getContentBT(){
    return content;
  }
};

class MsgServiceBT {
    
public: 
  MsgServiceBT(int rxPin, int txPin);  
  void initBT();  
  bool isMsgAvailableBT();
  MsgBT* receiveMsgBT();
  void sendMsgBT(MsgBT msg);
  
private:
  String content;
  MsgBT* availableMsg;
  SoftwareSerial* channel;
  
};

#endif