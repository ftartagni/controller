#ifndef LCDISPLAY_H
#define LCDISPLAY_H
#include "LiquidCrystal.h"
#define RS 12
#define EN 11
#define D4 5
#define D5 4
#define D6 3
#define D7 2

class LCDisplay {
  
  public:

  void init();
	void update(float temp);
};

extern LCDisplay lcdmanager;

#endif