#ifndef TASK_H
#define TASK_H

/**
 * A task that can be scheduled.
 */
class Task {
	int period;
	int timeElapsed;

  public:
	/**
	 * Initialize the task.
	 * @param period the period needed by the task to complete its work.
	 */
	void init(int period) {
		this->period = period;
		timeElapsed = 0;
	}

	/**
	 * Actions executed by the task when it's been scheduled.
	 */
	virtual void tick() = 0;

	/**
	 * Checks if the task has elapsed it's entire period time.
	 * @return if the task has elapsed it's entire period time.
	 */
	bool updateAndCheckTime(int basePeriod) {
		timeElapsed += basePeriod;
		if (timeElapsed >= period) {
			timeElapsed = 0;
			return true;
		} else {
			return false;
		}
	}
};
#endif
