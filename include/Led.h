#ifndef LED_H
#define LED_H

#include "Light.h"

/**
 * Implements a Led as a Light source.
 */
class Led : public Light {
  public:
	void switchOn() override;
	void switchOff() override;
	bool isTurnedOn() override;
};

#endif
