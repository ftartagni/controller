#ifndef TEMPCONTROLTASK_H
#define TEMPCONTROLTASK_H

#include "Task.h"
#include "MsgService.h"
#include "Led.h"

class TempControlTask : public Task {

  Led* led;

  public:

	explicit TempControlTask(Led* led);
	void init(int period);
	void tick() override;
};

#endif
