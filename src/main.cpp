#include <Arduino.h>
#include <LiquidCrystal.h>
#include "MsgService.h"
#include "MsgServiceBT.h"
#include "LedImpl.h"
#include "ThermostatGlobal.h"
#include "TempControlTask.h"
#include "ReadTask.h"
#include "ButtonTask.h"
#include "Scheduler.h"
#include "LCDisplay.h"

#define LED_PIN 10
#define BUTTON_PIN 8
#define BOUNCING_TIME 100

Scheduler scheduler;
Led* led;

void setup() {
  Serial.begin(9600);
  MsgService.init();
  scheduler.init(50);
  lcdmanager.init();
  led = new LedImpl(LED_PIN);
  pinMode(BUTTON_PIN, INPUT);

  ReadTask* readTask = new ReadTask();
  readTask->init(200);

  TempControlTask* tempControlTask = new TempControlTask(led);
  tempControlTask->init(2500);

  ButtonTask* buttonTask = new ButtonTask(led);
  buttonTask->init(BOUNCING_TIME);

  scheduler.addTask(readTask);
  scheduler.addTask(buttonTask);
  scheduler.addTask(tempControlTask);
}

void loop() {
  scheduler.schedule();
}
