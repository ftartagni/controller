#include "TempControlTask.h"
#include "ThermostatGlobal.h"
#include "MsgService.h"
#include "LedImpl.h"

TempControlTask::TempControlTask(Led* led) {
 	this->led = led;
	 if(ThermostatGlobal::state == ON) {
		 this->led->switchOn();
	 }
}

void
TempControlTask::init(int period) {
	Task::init(period);
}

void
TempControlTask::tick() {
	if (!ThermostatGlobal::isForcedOff && ThermostatGlobal::state == OFF && ThermostatGlobal::baseTemp - ThermostatGlobal::currTemp > 0.20 ) {
		/* turn on the led and send "ON" */
		MsgService.sendMsg(ThermostatGlobal::TurnOnSymbol);
		this->led->switchOn();
		ThermostatGlobal::state = StateType::ON;
		// Serial.print(F("PRIMO IF, baseTemp (Temperatura da raggiungere) = "));
		// Serial.println(ThermostatGlobal::baseTemp);
	}
	else if ( ThermostatGlobal::state == ON && ThermostatGlobal::currTemp - ThermostatGlobal::baseTemp > ThermostatGlobal::stepMax ) {
		/* turn OFF*/
		MsgService.sendMsg(ThermostatGlobal::TurnOffSymbol);
		this->led->switchOff();
		ThermostatGlobal::state = StateType::OFF;
		//Serial.println(F("ELSE IF, TURNED OFF"));
	}
	else {
		/* Keep doing what you were doing */
		 //MsgService.sendMsg("[ARDUINO] ... NOTHING CHANGED ...");
	}	
}
