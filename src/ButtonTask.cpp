#include "ButtonTask.h"
#include "ThermostatGlobal.h"
#include "ProfileType.h"
#include "LedImpl.h"
#include "MsgService.h"
#define BUTTON_PIN 8

ButtonTask::ButtonTask(Led* led) {
  this->led = led;
}

void
ButtonTask::init(int period) {
	Task::init(period);
}

void
ButtonTask::tick() {
  currButtonState = digitalRead(BUTTON_PIN);
  if (currButtonState != prevButtonState) {
    if (currButtonState == HIGH) {
       if ( ThermostatGlobal::isForcedOff ) {
        //Serial.println(F("Button Pressed, Turning On"));
        this->led->switchOn();
        ThermostatGlobal::isForcedOff = false;
        ThermostatGlobal::baseTemp = ThermostatGlobal::currTemp + 1;
      } else {
        //Serial.println(F("Button Pressed, Turning Off"));
        this->led->switchOff();
        ThermostatGlobal::isForcedOff = true;
        MsgService.sendMsg(ThermostatGlobal::TurnOffSymbol);
      }
    } else {
      //do nothing.
    }
  }
  prevButtonState = currButtonState;
}