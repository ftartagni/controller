#include "MsgServiceBT.h"
#include "Arduino.h"
#include "ThermostatGlobal.h"

MsgServiceBT::MsgServiceBT(int rxPin, int txPin){
  channel = new SoftwareSerial(rxPin, txPin);
}

void MsgServiceBT::initBT(){
  content.reserve(256);
  channel->begin(9600);
  availableMsg = NULL;
  content = "";
}

void MsgServiceBT::sendMsgBT(MsgBT msg){
  channel->println(msg.getContentBT());  
}

bool MsgServiceBT::isMsgAvailableBT(){
  while (channel->available()) {
    char ch = (char) channel->read();
    if (ch == ThermostatGlobal::endChar){
      availableMsg = new MsgBT(content); 
      content = "";
      return true;    
    } else {
      content += ch;      
    }
  }
  return false;  
}

MsgBT* MsgServiceBT::receiveMsgBT(){
  if (availableMsg != NULL){
    MsgBT* msg = availableMsg;
    availableMsg = NULL;
    return msg;  
  } else {
    return NULL;
  }
}
