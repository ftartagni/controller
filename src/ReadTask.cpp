#include "ReadTask.h"
#include "ThermostatGlobal.h"
#include "ProfileType.h"
#include "MsgService.h"
#include "MsgServiceBT.h"
#include "LCDisplay.h"

#define RX_PIN 7  // Connect to TX of the BT module.
#define TX_PIN 13  // Connect to RX of the BT module.
MsgServiceBT msgServiceBT(RX_PIN, TX_PIN);

ReadTask::ReadTask() {
	msgServiceBT.initBT();
}

void
ReadTask::init(int period) {
	Task::init(period);
}

void
ReadTask::tick() {

	if (msgServiceBT.isMsgAvailableBT()) {
		MsgBT* msgBt = msgServiceBT.receiveMsgBT();
		if (msgBt->getContentBT() == ThermostatGlobal::dayProfileSymbol) {
			ThermostatGlobal::profile = ProfileType::DAY;
			ThermostatGlobal::baseTemp = ThermostatGlobal::profileTemps[ThermostatGlobal::profile];
		} else if (msgBt->getContentBT() == ThermostatGlobal::nightProfileSymbol) {
			ThermostatGlobal::profile = ProfileType::NIGHT;
			ThermostatGlobal::baseTemp = ThermostatGlobal::profileTemps[ThermostatGlobal::profile];
		} else {
			//prendere versione con bottoni on e off, lasciarne uno e chiamarlo "OK"
			//così da app mando temperatura del number picker
			ThermostatGlobal::baseTemp = msgBt->getContentBT().toFloat();
			ThermostatGlobal::profileTemps[ThermostatGlobal::profile] = ThermostatGlobal::baseTemp;
		}
		delete msgBt;
  	}

	if (MsgService.isMsgAvailable()) {
    	Msg* msg = MsgService.receiveMsg();    
		ThermostatGlobal::currTemp = msg->getContent().toFloat();
		if (ThermostatGlobal::currTemp != prevTemp) {
			lcdmanager.update(ThermostatGlobal::currTemp);
			prevTemp = ThermostatGlobal::currTemp;
		}
    	/* NOT TO FORGET: message deallocation */
    	delete msg;
  	}
}
