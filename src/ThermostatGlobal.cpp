#include "ThermostatGlobal.h"

const String ThermostatGlobal::dayProfileSymbol = "DAY";
const String ThermostatGlobal::nightProfileSymbol = "NIGHT";
const String ThermostatGlobal::TurnOffSymbol = "OFF";
const String ThermostatGlobal::TurnOnSymbol = "ON";
const char ThermostatGlobal::endChar = '\n';
float ThermostatGlobal::currTemp = 24.6; // = 21.20;
const float ThermostatGlobal::stepMax = 1.10;
float ThermostatGlobal::dayMinTemp = 26.5;
float ThermostatGlobal::nightMinTemp = 19.00;
float ThermostatGlobal::profileTemps [] = {ThermostatGlobal::dayMinTemp, ThermostatGlobal::nightMinTemp};
StateType ThermostatGlobal::state = StateType::OFF;
ProfileType ThermostatGlobal::profile = ProfileType::DAY;
float ThermostatGlobal::baseTemp = profileTemps[ThermostatGlobal::profile];
bool ThermostatGlobal::isForcedOff = false;
