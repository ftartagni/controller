#include "LCDisplay.h"
#include "ThermostatGlobal.h"

LCDisplay lcdmanager;
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void LCDisplay::init() {
    lcd.begin(16, 2);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(String(ThermostatGlobal::currTemp));
    //lcd.print("IOThermostat");
}

void LCDisplay::update(float temp) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(String(ThermostatGlobal::currTemp));
}